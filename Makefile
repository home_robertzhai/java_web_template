build:
	mvn package -Dmaven.test.skip=true

clean:
	mvn clean

tests:
	mvn test

dev: build
	java -Xms1024m -Xmx1024m -XX:+UseG1GC  -jar -Dspring.profiles.active=dev   target/java_web_template-1.0.0.jar

test: build
	java -Xms1024m -Xmx1024m -XX:+UseG1GC  -jar -Dspring.profiles.active=test  target/java_web_template-1.0.0.jar

prod: build
	java -Xms8192m -Xmx8192m -XX:+UseG1GC  -jar -Dspring.profiles.active=prod   target/java_web_template-1.0.0.jar

docker_start: build
	docker-compose -f docker/docker-compose.yml up -d --force-recreate --build

docker_stop:
	docker-compose -f docker/docker-compose.yml down
