create database javaweb;

CREATE TABLE `register_user` (
                                 `id` int NOT NULL AUTO_INCREMENT,
                                 `name` varchar(255) DEFAULT NULL,
                                 PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4;

insert register_user set name='robertzhai';
insert register_user set name='admin';

CREATE TABLE `log` (
                            `id` int NOT NULL AUTO_INCREMENT,
                            `info` varchar(255) DEFAULT NULL,
                            `cts` datetime,
                            `uts` datetime,
                            PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;