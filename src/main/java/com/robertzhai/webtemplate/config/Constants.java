package com.robertzhai.webtemplate.config;

public class Constants {
    public static final int SuccCode = 1000;
    public static final int ParamsInvalidCode = 1001;
    public static final String ParamsInvalidMsg = "参数错误";
    public static final int SystemErrorCode = 1002;
    public static final String SystemErrorMsg= "系统繁忙，请稍后再试";

    public static final String Prod = "prod";

}
