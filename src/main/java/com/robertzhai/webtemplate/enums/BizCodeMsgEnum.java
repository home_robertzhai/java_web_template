package com.robertzhai.webtemplate.enums;

import com.robertzhai.webtemplate.config.Constants;

public enum BizCodeMsgEnum {


    SUCC(Constants.SuccCode,""),
    PARAM_EXCEPTION(Constants.ParamsInvalidCode,Constants.ParamsInvalidMsg),
    SYSTEM_ERROR_EXCEPTION(Constants.SystemErrorCode,Constants.SystemErrorMsg);

    private int code;
    private String message;

     BizCodeMsgEnum(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
