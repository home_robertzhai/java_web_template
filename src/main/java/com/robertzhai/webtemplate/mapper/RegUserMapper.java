package com.robertzhai.webtemplate.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.robertzhai.webtemplate.entity.RegUser;

public interface RegUserMapper extends BaseMapper<RegUser> {

    IPage<RegUser> selectPageList(Page<?> page);

}
