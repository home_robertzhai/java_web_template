package com.robertzhai.webtemplate.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.robertzhai.webtemplate.entity.RegUser;
import com.robertzhai.webtemplate.mapper.RegUserMapper;
import com.robertzhai.webtemplate.service.RegUserService;
import org.springframework.stereotype.Service;

@Service
public class RegUserServiceImpl extends ServiceImpl<RegUserMapper, RegUser> implements RegUserService {

    @Override
    public IPage<RegUser> selectUserPageList(Page<RegUser> page) {
        return baseMapper.selectPageList(page);
    }
}
