package com.robertzhai.webtemplate.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.robertzhai.webtemplate.entity.RegUser;


public interface RegUserService extends IService<RegUser> {

    IPage<RegUser> selectUserPageList(Page<RegUser> page);

}

