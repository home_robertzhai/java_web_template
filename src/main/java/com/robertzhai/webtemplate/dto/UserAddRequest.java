package com.robertzhai.webtemplate.dto;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserAddRequest {

//    @Min(value = 1, message = "id不能<1")
//    @NotNull(message = "id 不能为空")
//    private Integer id;


    @Length(min=1,max = 30,message = "name长度非法")
    @NotEmpty(message = "name 不能为空")
    private String name;




}
