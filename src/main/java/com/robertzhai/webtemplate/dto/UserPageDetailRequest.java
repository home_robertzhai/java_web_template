package com.robertzhai.webtemplate.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserPageDetailRequest {

    @Min(value = 1, message = "id不能<1")
    @NotNull(message = "id 不能为空")
    private Integer id;



}
