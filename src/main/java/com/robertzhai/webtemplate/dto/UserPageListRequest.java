package com.robertzhai.webtemplate.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserPageListRequest {

    @Min(value = 1, message = "page不能<1")
    @NotNull(message = "page 不能为空")
    private Integer page;

    @Min(value = 1, message = "pagesize不能<1")
    @Max(value = 100, message = "pagesize不能>100")
    @NotNull(message = "pagesize 不能为空")
    private Integer pagesize;


}
