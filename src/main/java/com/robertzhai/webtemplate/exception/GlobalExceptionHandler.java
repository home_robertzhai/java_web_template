package com.robertzhai.webtemplate.exception;

import com.robertzhai.webtemplate.config.Constants;
import com.robertzhai.webtemplate.utils.JsonResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.stream.Collectors;

@Slf4j
@RestControllerAdvice
public class GlobalExceptionHandler {
    /**
     * @RequestBody 上校验失败后抛出的异常是 MethodArgumentNotValidException 异常。
     */
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public JsonResult<Object> handleMethodArgumentNotValidException(MethodArgumentNotValidException e) {
        BindingResult bindingResult = e.getBindingResult();
        String messages = bindingResult.getAllErrors()
                .stream()
                .map(ObjectError::getDefaultMessage)
                .collect(Collectors.joining("；"));
        JsonResult<Object> result = new JsonResult<Object>();
        result.setCode(Constants.ParamsInvalidCode);
        result.setMessage(messages);
        return result;
    }
    /**
     * 不加 @RequestBody注解，校验失败抛出的则是 BindException
     */
    @ExceptionHandler(value = BindException.class)
    public JsonResult<Object> exceptionHandler(BindException e){
        String messages = e.getBindingResult().getAllErrors()
                .stream()
                .map(ObjectError::getDefaultMessage)
                .collect(Collectors.joining("；"));
        JsonResult<Object> result = new JsonResult<Object>();
        result.setCode(Constants.ParamsInvalidCode);
        result.setMessage(messages);
        return result;
    }

    /**
     *  @RequestParam 上校验失败后抛出的异常是 ConstraintViolationException
     */
    @ExceptionHandler({ConstraintViolationException.class})
    public JsonResult<Object> methodArgumentNotValid(ConstraintViolationException exception) {
        String message = exception.getConstraintViolations().stream().map(ConstraintViolation::getMessage).collect(Collectors.joining("；"));
        JsonResult<Object> result = new JsonResult<Object>();
        result.setCode(Constants.ParamsInvalidCode);
        result.setMessage(message);
        return result;
    }

    /**
     * 空指针异常
     * @param ex NullPointerException
     * @return
     */
    @ExceptionHandler(NullPointerException.class)
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    public JsonResult<Object> handleTypeMismatchException(NullPointerException ex) {
        JsonResult<Object> result = new JsonResult<Object>();
        result.setCode(Constants.SystemErrorCode);
        result.setMessage(Constants.SystemErrorMsg);
        return result;
    }

    @ExceptionHandler(BizErrorException.class)
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    public JsonResult<Object> handleTypeBizException(BizErrorException ex) {
        log.info("handleTypeBizException" + ex.getMessage());
        JsonResult<Object> result = new JsonResult<Object>();
        result.setCode(ex.getCode());
        result.setMessage(ex.getMessage());
        return result;
    }


    @ExceptionHandler(Exception.class)
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    public JsonResult<Object> handleException(Exception ex) {

        log.info("handleException" + ex.getMessage());
        JsonResult<Object> result = new JsonResult<Object>();
        result.setCode(Constants.SystemErrorCode);
        result.setMessage(Constants.SystemErrorMsg);
        return result;
    }


}
