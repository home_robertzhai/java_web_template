package com.robertzhai.webtemplate.exception;


import com.robertzhai.webtemplate.enums.BizCodeMsgEnum;

public class BizErrorException  extends RuntimeException  {

    private static final long serialVersionUID = 1L;

    private int code;
    private String message;

    public BizErrorException(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public BizErrorException(BizCodeMsgEnum biz) {
        this.code = biz.getCode();
        this.message = biz.getMessage();
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
