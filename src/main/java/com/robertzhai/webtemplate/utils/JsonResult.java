package com.robertzhai.webtemplate.utils;

import com.robertzhai.webtemplate.config.Constants;
import com.robertzhai.webtemplate.enums.BizCodeMsgEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
public class JsonResult<T> {
    public int code;
    public String message;
    public T data;

    public JsonResult() {
    }

    public JsonResult(BizCodeMsgEnum msg) {
        code = msg.getCode();
        message = msg.getMessage();
    }

    public JsonResult(T data) {
        code = Constants.SuccCode;
        this.data = data;
    }
}
