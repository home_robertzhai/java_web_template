package com.robertzhai.webtemplate.cron;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.text.SimpleDateFormat;
import java.util.Date;

@Component
public class DemoJob {
    private static final Logger log = LoggerFactory.getLogger(DemoJob.class);
    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

    @Scheduled(fixedRate = 5000)
    public void reportTime() {
        log.info("start job time is {}", dateFormat.format(new Date()));
    }
}
