package com.robertzhai.webtemplate.controller.api;

import com.robertzhai.webtemplate.config.Env;
import com.robertzhai.webtemplate.dto.ConfResponse;
import com.robertzhai.webtemplate.dto.DemoRequest;
import com.robertzhai.webtemplate.dto.DemoResponse;
import com.robertzhai.webtemplate.exception.BizErrorException;
import com.robertzhai.webtemplate.enums.BizCodeMsgEnum;
import com.robertzhai.webtemplate.utils.JsonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;

@Slf4j
@RestController
@RequestMapping("/api/demo")
@Validated
@Api(value = "demo测试接口", tags = "demo相关的接口")
public class DemoController {


    @Autowired
    private Env env;

    /**
     * @param personRequest
     * @return
     */
    @ApiOperation(value = "新增用户", notes = "新增用户信息")
    @PostMapping("/add")
    public JsonResult<DemoRequest> add(@Valid DemoRequest personRequest) {
        JsonResult result = new JsonResult(BizCodeMsgEnum.SUCC);
        result.setData(personRequest);
        return result;
    }

    /**
     * @param name
     * @param email
     * @return
     */
    @ApiOperation(value = "查询用户信息", notes = "根据用户名和邮箱查询用户信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "name", value = "用户名", required = true, dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "email", value = "邮箱", required = true, dataType = "String", paramType = "query"),
    })
    @GetMapping("/query")
    public JsonResult<DemoResponse> query(@NotBlank(message = "名称不能为空") String name, @NotBlank(message = "邮箱不能为空") String email) {
        JsonResult result = new JsonResult(BizCodeMsgEnum.SUCC);
        DemoResponse data = new DemoResponse();
        data.setEmail(email);
        data.setName(name);
        result.setData(data);
        return result;
    }

    /**
     * @return
     */
    @GetMapping("/conf")
    public JsonResult<ConfResponse> conf() {
        JsonResult result = new JsonResult(BizCodeMsgEnum.SUCC);
        ConfResponse data = new ConfResponse();
        data.setEnv(env.getEnv());
        result.setData(data);
        return result;
    }


    /**
     * @return
     */
    @GetMapping("/log")
    public JsonResult<ConfResponse> log() {
        log.debug("start debug log");
        log.info("start info log");
        ConfResponse data = new ConfResponse();
        data.setEnv(env.getEnv());
        log.debug("end debug log");
        log.info("end info log =======");
        return new JsonResult<>(data);
    }

    /**
     * @return
     */
    @GetMapping("/biz")
    public JsonResult<Object> bizExp() {
        try {

            int i = 1/0;
        } catch (Exception e) {
            throw new BizErrorException(BizCodeMsgEnum.SYSTEM_ERROR_EXCEPTION);
        }
        return new JsonResult<>(null);
    }

    /**
     * @return
     */
    @GetMapping("/exp")
    public JsonResult<Object> defaultExp() throws Exception {
        try {
            int i = 1/0;
        } catch (Exception e) {
            throw new Exception("exp test");
        }
        return new JsonResult<>(null);
    }

    @GetMapping("/echo")
    public JsonResult<Object> echo() {
        JsonResult<Object> ret = new JsonResult<Object>();
        ret.setData(null);
        return ret;
    }
}
