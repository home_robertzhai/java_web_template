package com.robertzhai.webtemplate.controller.admin;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Api(value = "tpl admin", tags = "tpl admin")
@Controller
@RequestMapping("/admin/tpl")
public class TplController {

    @ApiOperation(value = "home page", notes = "home page")
    @GetMapping("/home")
    public String home(ModelMap map) {
        map.addAttribute("username", "robertzhai");
        map.addAttribute("address", "https://gitee.com/home_robertzhai/index");
        return "home";
    }
}
