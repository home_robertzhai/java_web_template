package com.robertzhai.webtemplate.controller.admin;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.api.R;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.robertzhai.webtemplate.config.Constants;
import com.robertzhai.webtemplate.dto.UserAddRequest;
import com.robertzhai.webtemplate.dto.UserDelRequest;
import com.robertzhai.webtemplate.dto.UserPageDetailRequest;
import com.robertzhai.webtemplate.dto.UserPageListRequest;
import com.robertzhai.webtemplate.entity.RegUser;
import com.robertzhai.webtemplate.service.RegUserService;
import com.robertzhai.webtemplate.utils.JsonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@Api(value = "用户测试接口", tags = "mybaits-plus相关的接口")
@RestController
@RequestMapping("/api/user")
@Validated
public class UserController {

    @Autowired
    private RegUserService userService;


    @ApiOperation(value = "新增用户", notes = "新增用户信息")
    @PostMapping("/add")
    public JsonResult<Object> saveUser(@Valid UserAddRequest userAddRequest ) {
        JsonResult<Object> result = new JsonResult<Object>();
        RegUser user = new RegUser();
        user.setName(userAddRequest.getName());
        boolean addRet = userService.save(user);
        if (addRet) {
            result.setCode(Constants.SuccCode);
        } else {
            result.setCode(Constants.SystemErrorCode);
            result.setMessage(Constants.SystemErrorMsg);
        }
        return result;
    }

    @ApiOperation(value = "删除用户", notes = "删除用户信息")
    @PostMapping("/del")
    public JsonResult<Object> delUser(@Valid UserDelRequest userDelRequest) {
        JsonResult<Object> result = new JsonResult<Object>();
        boolean addRet = userService.removeById(userDelRequest.getId());
        if (addRet) {
            result.setCode(Constants.SuccCode);
        } else {
            result.setCode(Constants.SystemErrorCode);
            result.setMessage(Constants.SystemErrorMsg);
        }
        return result;
    }

    @ApiOperation(value = "用户详情", notes = "用户详情信息")
    @GetMapping("/detail")
    public JsonResult<RegUser> getUser(@Valid UserPageDetailRequest userPageDetailRequest) {
        JsonResult<RegUser> result = new JsonResult<RegUser>();
        result.setData(userService.getById(userPageDetailRequest.getId()));
        return result;
    }

    @ApiOperation(value = "用户列表", notes = "用户列表信息")
    @GetMapping("/list")
    public JsonResult<List<RegUser>> getUserList() {
        JsonResult<List<RegUser>> result = new JsonResult<List<RegUser>>();
        result.setData(userService.list());
        return result;
    }

    @ApiOperation(value = "用户分页查询", notes = "用户分页列表信息")
    @GetMapping("/page/list")
    public JsonResult<IPage<RegUser>> getPageList(@Valid UserPageListRequest userPageListRequest) {
        Page<RegUser> userPage = new Page<RegUser>();
        userPage.setCurrent(userPageListRequest.getPage());//设置页数
        userPage.setSize(userPageListRequest.getPagesize()); //每页条数
        IPage<RegUser> userIPage = userService.selectUserPageList(userPage);
        JsonResult<IPage<RegUser>> result = new JsonResult<IPage<RegUser>>();
        result.setData(userIPage);
        return result;
    }
}
