echo "start gen empty app code....."
app_name=${1}
app_name_no_dash=${app_name//_/}
echo "usage:sh scripts/folder/gen_java_server_project.sh ${app_name} \n"
echo "app_name:${app_name}"
echo "app_name_no_dash:${app_name_no_dash}"
echo "s#java_web_template#${app_name}#g"
read -p "confirm app_name: ${app_name}[Yy]:" -n 1 -r
echo # (optional) move to a new line
if [[ $REPLY =~ [Yy]$ ]]; then
  echo "confirmed"
  rm -rf java_web_template
  git clone git@gitee.com:home_robertzhai/java_web_template.git
  mv java_web_template ${app_name}
  cd ${app_name}
  rm -rf .git
  sed -i "" "s#java_web_template#${app_name}#g" `grep java_web_template -rl .`
  sed -i "" "s#javawebtemplate#${app_name_no_dash}#g" `grep javawebtemplate -rl .`
  rm -rf shell
  mvn clean package
else
  echo "nothing to do"
fi

echo "end gen folder"
