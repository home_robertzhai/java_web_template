# java_web_template

#### 介绍
- 一键快速生成spring boot 的api server project 示例代码，提示研发效能。
- 当前版本:v1.0.0

### java版本
- jdk: 1.8
- Spring Boot-v2.5.6
- mvn 3.6
```
  <mirrors>

 <mirror>
    <id>aliyun</id>
    <mirrorOf>central</mirrorOf>
    <name>aliyun</name>
    <url>https://maven.aliyun.com/repository/public</url>
</mirror>

<mirror>
    <id>huaweicloud</id>
    <name>mirror from maven huaweicloud</name>
    <mirrorOf>central</mirrorOf>
    <url>https://repo.huaweicloud.com/repository/maven/</url>
</mirror>
  </mirrors>
 ```   

### log
- logback

### profile dev
- make dev

### profile test
- make test

### profile prod
- make prod

### profile ide中配置 program arguments
- --spring.profiles.active=dev
- --spring.profiles.active=test
- --spring.profiles.active=prod

### 使用说明
- git clone git@gitee.com:home_robertzhai/java_web_template.git
- make api && cd api
- sh ../java_web_template/shell/gen_java_server_project.sh java_api

### layout
```

.
├── LICENSE
├── Makefile
├── README.md
├── docker
│   ├── Dockerfile
│   ├── docker-compose-mysql8.yml
│   ├── docker-compose.yml
├── docs
│   ├── docker.md
│   └── javaweb.sql
├── java_tutorials.iml
├── pom.xml
├── shell
│   └── gen_java_server_project.sh
└── src
    ├── main
    │   ├── java
    │   │   └── com
    │   │       └── robertzhai
    │   │           └── webtemplate
    │   │               ├── MainApplication.java
    │   │               ├── config
    │   │               │   ├── Constants.java
    │   │               │   ├── Env.java
    │   │               │   ├── ParamValidatorConfig.java
    │   │               │   └── SwaggerConfig.java
    │   │               ├── controller
    │   │               │   ├── admin
    │   │               │   │   ├── TplController.java
    │   │               │   │   └── UserController.java
    │   │               │   └── api
    │   │               │       └── DemoController.java
    │   │               ├── cron
    │   │               │   └── DemoJob.java
    │   │               ├── dto
    │   │               │   ├── ConfResponse.java
    │   │               │   ├── DemoRequest.java
    │   │               │   ├── DemoResponse.java
    │   │               │   ├── UserAddRequest.java
    │   │               │   ├── UserDelRequest.java
    │   │               │   ├── UserPageDetailRequest.java
    │   │               │   └── UserPageListRequest.java
    │   │               ├── entity
    │   │               │   └── RegUser.java
    │   │               ├── enums
    │   │               │   └── BizCodeMsgEnum.java
    │   │               ├── exception
    │   │               │   ├── BizErrorException.java
    │   │               │   └── GlobalExceptionHandler.java
    │   │               ├── generator
    │   │               │   └── CodeGenerator.java
    │   │               ├── mapper
    │   │               │   └── RegUserMapper.java
    │   │               ├── service
    │   │               │   ├── RegUserService.java
    │   │               │   └── impl
    │   │               │       └── RegUserServiceImpl.java
    │   │               └── utils
    │   │                   └── JsonResult.java
    │   └── resources
    │       ├── application-dev.properties
    │       ├── application-prod.properties
    │       ├── application-test.properties
    │       ├── application.properties
    │       ├── logback-spring-dev.xml
    │       ├── logback-spring-prod.xml
    │       ├── mapper
    │       │   └── RegUserMapper.xml
    │       ├── static
    │       └── templates
    │           └── home.html
```

### mysql、es、redis等集成样例project
- https://gitee.com/home_robertzhai/java_tutorials

### docker start
- make docker_start

### docker stop  
- make docker_stop

### jdk 8下载
- https://www.oracle.com/java/technologies/downloads/#java8

### jdk 环境变量配置
- linux
```
export JAVA_HOME=/java/jdk8
export PATH=$JAVA_HOME/bin:$PATH
export CLASSPATH=.:$JAVA_HOME/lib/tools.jar:$JAVA_HOME/lib/dt.jar
```
- windows 配置环境变量

### swagger-ui
-  http://localhost:8080/jwt/doc.html
-  http://localhost:8080/jwt/swagger-ui.html





